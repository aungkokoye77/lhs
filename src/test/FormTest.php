<?php

namespace Test;


use App\Form;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class FormTest extends TestCase
{
    /**
     * @dataProvider formDataProvider
     * @param $filename
     * @param $year
     * @param $expect
     * @param $errors
     */
    public function testConstructor($filename, $year, $expect, $errors)
    {

        $request = Request::createFromGlobals();
        $request->setMethod(Request::METHOD_POST);
        $request->request->set('file_name', $filename);
        $request->request->set('year', $year);

        $form = new Form($request);
        $this->assertEquals($expect, $form->validate());
        $this->assertEquals($errors, $form->errors);

    }

    /**
     * @return array
     * @throws \Exception
     */
    public function formDataProvider(): array
    {
        $interval = new \DateInterval('P1Y');
        $dateTime = new \DateTime();

        return [
            ['test_year', '2000', true, []],
            ['test_year', null, false, ["Year should not be empty!"]],
            ['test_year', '1999', false, ["Not Valid Year!"]],
            ['test_year', $dateTime->add($interval)->format('Y'), false, ["Not Valid Year!"]],

            [null, '2000', false, ["File Name should not be empty!"]],
            ['123456789012345678901', '2000', false, ["File Name is too long!"]],

            [null, null, false, ["File Name should not be empty!", "Year should not be empty!"]],
            ['test_file_name_tear_1234567890', 1999, false, ["File Name is too long!", "Not Valid Year!"]],
        ];
    }


}