<?php


namespace Test;


use App\Report;
use App\Form;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ReportTest extends TestCase
{

    /**
     * @dataProvider generateReportDataProvider
     *
     * @param $expect
     * @param $year
     * @param $filename
     * @throws \Exception
     */
    public function testGenerateReport($expect, $year, $filename)
    {
        $request = Request::createFromGlobals();
        $request->setMethod(Request::METHOD_POST);
        $request->request->set('file_name', $filename);
        $request->request->set('year', $year);

        $form = new Form($request);

        $report = new Report($form);
        $this->assertEquals($expect, $report->generateReport());
    }


    /**
     * @dataProvider exceptionThrowDataProvider
     *
     * @param $year
     * @throws \Exception
     */
    public function testExceptionThrow($year)
    {

        $request = Request::createFromGlobals();
        $request->setMethod(Request::METHOD_POST);
        $request->request->set('file_name', 'test_1');
        $request->request->set('year', $year);

        $form = new Form($request);

        $this->expectException(\Exception::class);
        new Report($form);
    }

    public function exceptionThrowDataProvider()
    {
        $interval = new \DateInterval('P1Y');
        $dateTime = new \DateTime();

        return [
            [1999],
            [null],
            [$dateTime->add($interval)->format('Y')],
            ['here is year'],
        ];
    }

    /**
     * @return array
     */
    public function generateReportDataProvider(): array
    {
        return [
            [
                "Month Name, 1st expenses date, second expense date, salary day 
January, 2000-01-03, 2000-01-17, 2000-01-31
February, 2000-02-01, 2000-02-15, 2000-02-29
March, 2000-03-01, 2000-03-15, 2000-03-31
April, 2000-04-03, 2000-04-17, 2000-04-28
May, 2000-05-01, 2000-05-15, 2000-05-31
June, 2000-06-01, 2000-06-15, 2000-06-30
July , 2000-07-03, 2000-07-17, 2000-07-31
August, 2000-08-01, 2000-08-15, 2000-08-31
September, 2000-09-01, 2000-09-15, 2000-09-29
October, 2000-10-02, 2000-10-16, 2000-10-31
November, 2000-11-01, 2000-11-15, 2000-11-30
December, 2000-12-01, 2000-12-15, 2000-12-29
"
                ,
                2000,
                'test_1'
            ],

            [
                "Month Name, 1st expenses date, second expense date, salary day 
January, 2003-01-01, 2003-01-15, 2003-01-31
February, 2003-02-03, 2003-02-17, 2003-02-28
March, 2003-03-03, 2003-03-17, 2003-03-31
April, 2003-04-01, 2003-04-15, 2003-04-30
May, 2003-05-01, 2003-05-15, 2003-05-30
June, 2003-06-02, 2003-06-16, 2003-06-30
July , 2003-07-01, 2003-07-15, 2003-07-31
August, 2003-08-01, 2003-08-15, 2003-08-29
September, 2003-09-01, 2003-09-15, 2003-09-30
October, 2003-10-01, 2003-10-15, 2003-10-31
November, 2003-11-03, 2003-11-17, 2003-11-28
December, 2003-12-01, 2003-12-15, 2003-12-31
"

                ,
                2003,
                'test_2'
            ],

            [
                "Month Name, 1st expenses date, second expense date, salary day 
January, 2009-01-01, 2009-01-15, 2009-01-30
February, 2009-02-02, 2009-02-16, 2009-02-27
March, 2009-03-02, 2009-03-16, 2009-03-31
April, 2009-04-01, 2009-04-15, 2009-04-30
May, 2009-05-01, 2009-05-15, 2009-05-29
June, 2009-06-01, 2009-06-15, 2009-06-30
July , 2009-07-01, 2009-07-15, 2009-07-31
August, 2009-08-03, 2009-08-17, 2009-08-31
September, 2009-09-01, 2009-09-15, 2009-09-30
October, 2009-10-01, 2009-10-15, 2009-10-30
November, 2009-11-02, 2009-11-16, 2009-11-30
December, 2009-12-01, 2009-12-15, 2009-12-31
"
                ,
                2009,
                'test_3'
            ],

            [
                "Month Name, 1st expenses date, second expense date, salary day 
January, 2012-01-02, 2012-01-16, 2012-01-31
February, 2012-02-01, 2012-02-15, 2012-02-29
March, 2012-03-01, 2012-03-15, 2012-03-30
April, 2012-04-02, 2012-04-16, 2012-04-30
May, 2012-05-01, 2012-05-15, 2012-05-31
June, 2012-06-01, 2012-06-15, 2012-06-29
July , 2012-07-02, 2012-07-16, 2012-07-31
August, 2012-08-01, 2012-08-15, 2012-08-31
September, 2012-09-03, 2012-09-17, 2012-09-28
October, 2012-10-01, 2012-10-15, 2012-10-31
November, 2012-11-01, 2012-11-15, 2012-11-30
December, 2012-12-03, 2012-12-17, 2012-12-31
"
                ,
                2012,
                'test_4'
            ],

            [
                "Month Name, 1st expenses date, second expense date, salary day 
January, 2015-01-01, 2015-01-15, 2015-01-30
February, 2015-02-02, 2015-02-16, 2015-02-27
March, 2015-03-02, 2015-03-16, 2015-03-31
April, 2015-04-01, 2015-04-15, 2015-04-30
May, 2015-05-01, 2015-05-15, 2015-05-29
June, 2015-06-01, 2015-06-15, 2015-06-30
July , 2015-07-01, 2015-07-15, 2015-07-31
August, 2015-08-03, 2015-08-17, 2015-08-31
September, 2015-09-01, 2015-09-15, 2015-09-30
October, 2015-10-01, 2015-10-15, 2015-10-30
November, 2015-11-02, 2015-11-16, 2015-11-30
December, 2015-12-01, 2015-12-15, 2015-12-31
"
                ,
                2015,
                'test_5'
            ],

            [
                "Month Name, 1st expenses date, second expense date, salary day 
January, 2019-01-01, 2019-01-15, 2019-01-31
February, 2019-02-01, 2019-02-15, 2019-02-28
March, 2019-03-01, 2019-03-15, 2019-03-29
April, 2019-04-01, 2019-04-15, 2019-04-30
May, 2019-05-01, 2019-05-15, 2019-05-31
June, 2019-06-03, 2019-06-17, 2019-06-28
July , 2019-07-01, 2019-07-15, 2019-07-31
August, 2019-08-01, 2019-08-15, 2019-08-30
September, 2019-09-02, 2019-09-16, 2019-09-30
October, 2019-10-01, 2019-10-15, 2019-10-31
November, 2019-11-01, 2019-11-15, 2019-11-29
December, 2019-12-02, 2019-12-16, 2019-12-31
"
                ,
                2019,
                'test_6'
            ]
        ];
    }


}