<?php


namespace App;

class FileDownload
{

    private $form;
    private $report;

    /**
     * FileDownload constructor.
     *
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
        $this->form   = $report->getForm();
    }

    /**
     * @throws \Exception
     */
    public function download()
    {
        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename={$this->form->filename}.txt");

        try {

            print $this->report->generateReport();
            exit();

        } catch (\Exception $e) {
            throw new \Exception("Error: {$e->getMessage()}");
        }

    }

}