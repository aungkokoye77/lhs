<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;

class Form
{
    const FILE_NAME_LENGTH = 20;
    const YEAR_START       = 2000;

    public $year;
    public $filename;
    public $errors;

    public $formNameField = array (
        'label' => 'File Name',
        'name'  => 'file_name'
    );

    public $yearField = array (
        'label' => 'Year',
        'name'  => 'year'
    );

    /**
     * Form constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        if ($request->isMethod(Request::METHOD_POST)) {

            $request        = $request->request;
            $this->filename = $request->get($this->formNameField['name']);
            $this->year     = $request->get($this->yearField['name']);

        }

        $this->errors = array ();
    }

    /**
     * validate form submit
     * @return bool
     */
    public function validate(): bool
    {
        $this->validateFileName();
        $this->validateYear();
        return empty($this->errors) ? true : false;

    }

    /**
     * validate form_name field
     */
    private function validateFileName()
    {

        if (empty($this->filename)) {
            $this->errors[] = "{$this->formNameField['label']} should not be empty!";
        } elseif (strlen($this->filename) > self::FILE_NAME_LENGTH) {
            $this->errors[] = "{$this->formNameField['label']} is too long!";
        }

    }

    /**
     * validate year field
     */
    private function validateYear()
    {

        if (empty($this->year)) {

            $this->errors[] = "{$this->yearField['label']} should not be empty!";

        } elseif (preg_match('/^[1-2][0-9][0-9][0-9]$/', $this->year) !== 1 ||
            $this->year < self::YEAR_START ||
            $this->year > date('Y')) {

            $this->errors[] = "Not Valid {$this->yearField['label']}!";

        }

    }


}