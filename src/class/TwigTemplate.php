<?php

namespace App;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigTemplate
{

    Const PATH = './template';

    private static $twig;

    /**
     * @return Environment
     */
    public static function getTwig(): Environment
    {

        if (!self::$twig instanceof Environment) {
            $loader     = new FilesystemLoader(self::PATH);
            self::$twig = new Environment($loader, []);
        }

        return self::$twig;
    }

}