<?php

namespace App;

use Exception;

class Report
{
    const YEAR_START = 2000;

    private $form;

    private $months = array (
        'January'   => '01',
        'February'  => '02',
        'March'     => '03',
        'April'     => '04',
        'May'       => '05',
        'June'      => '06',
        'July '     => '07',
        'August'    => '08',
        'September' => '09',
        'October'   => '10',
        'November'  => '11',
        'December'  => '12',
    );

    /**
     * Report constructor.
     * @param Form $form
     * @throws Exception
     */
    public function __construct(Form $form)
    {

        if (!$form->validate()) {

            throw new Exception('Year is not valid.');

        }

        $this->form = $form;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function generateReport()
    {
        $string = "Month Name, 1st expenses date, second expense date, salary day \n";

        foreach ($this->months as $monthName => $month) {
            $firstExpenseDate     = "{$this->form->year}-{$month}-01";
            $firstExpenseDateTime = new \DateTime($firstExpenseDate);

            $secondExpenseDate     = "{$this->form->year}-{$month}-15";
            $secondExpenseDateTime = new \DateTime($secondExpenseDate);

            $salaryDate     = "{$this->form->year}-{$month}-{$firstExpenseDateTime->format('t')}";
            $salaryDateTime = new \DateTime($salaryDate);

            $string .= "{$monthName}, ";
            $string .= "{$this->expenseDate($firstExpenseDateTime)}, ";
            $string .= "{$this->expenseDate($secondExpenseDateTime)}, ";
            $string .= "{$this->salaryDate($salaryDateTime)}";
            $string .= "\n";
        }

        return $string;
    }

    /**
     * @param \DateTime $date
     * @return bool|string
     * @throws Exception
     */
    private function isWeekend(\DateTime $date)
    {
        $day = $date->format('N');

        if ($day > 5) {
            return $day;
        }

        return false;
    }

    /**
     * @param \DateTime $date
     * @return bool|string
     * @throws Exception
     */
    private function expenseDate(\DateTime $date)
    {

        $day = $this->isWeekend($date);

        if ($day === false) {
            return $date->format('Y-m-d');
        }

        if ($day == 6) {
            $date->add(new \DateInterval('P2D'));
        } else {
            $date->add(new \DateInterval('P1D'));
        }

        return $date->format('Y-m-d');

    }

    /**
     * @param \DateTime $date
     * @return string
     * @throws Exception
     */
    private function salaryDate(\DateTime $date)
    {
        $day = $this->isWeekend($date);

        if ($day === false) {
            return $date->format('Y-m-d');
        }

        if ($day == 6) {
            $date->sub(new \DateInterval('P1D'));
        } else {
            $date->sub(new \DateInterval('P2D'));
        }

        return $date->format('Y-m-d');
    }

    /**
     * @return Form
     */
    public function getForm()
    {
        return $this->form;
    }

}