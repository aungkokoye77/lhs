<?php

require_once './vendor/autoload.php';

use App\FileDownload;
use App\Form;
use App\Report;
use App\TwigTemplate;
use Symfony\Component\HttpFoundation\Request;

$twig    = TwigTemplate::getTwig();
$request = Request::createFromGlobals();
$form    = new Form($request);

if ($request->isMethod(Request::METHOD_POST) && $form->validate()) {

    $fileDownload = new FileDownload(new Report($form));
    $fileDownload->download();

}

echo $twig->render('index.html.twig', ['form' => $form]);




