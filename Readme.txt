Company Ltd with many branches across Europe handle their sales payroll in the following way:

The normal base salaries are paid on the last day of the month unless that day is a Saturday or a Sunday (weekend); if this is the case they will be paid on the last working day of the month. 

On the 1st and 15th of each month the expenses are paid for the previous fortnight, unless those days are on a weekend, in which case they are paid on the first Monday after that date.

The company needs the output in the following format:

"Month Name", "1st expenses day", “2nd expenses day”, "Salary day" 

"January", "YYYY-MM-DD", "YYYY-MM-DD", "YYYY-MM-DD" 

"February", "YYYY-MM-DD", "YYYY-MM-DD", "YYYY-MM-DD" 



.........

Your task is to create an application, which implements this code flow and can output the pay dates for any given year.
This application will output a to a file.


[Instruction]

lhs is project root folder.
src is application root folder.

Landing page is ./src/index.php
Url : localhost:8500/

[Docker]

$cd lhs;
$docker-compose up --build (run this command where docker-compose.yml exist)

[Composer]

Need to install composer
$ cd ./src
$composer install (run this command where composer.json exist)


[PHP Unit Test]

All test live in src/test folder

$ cd ./src
$ ./vendor/bin/phpunit --coverage-html coverage (run this command where phpunit.xml exist).


[PHP Unit Test coverage]

Coverage html file  ./src/coverage
Url: localhost:8500/coverage/



